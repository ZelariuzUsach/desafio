import React, { useState, Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

import ReactECharts from 'echarts-for-react';  // or var ReactECharts = require('echarts-for-react');

import './App.css';

/*
  Notas :
    - La app no esta optimizada
    - La app no tiene soporte de Error en XHR (Request)
    - La app no esta separada en archivos componentes
*/

class MyVerticallyCenteredModal extends Component {

  state = {
    g : '',
    gdata : {},
    rg : '',
    rgdata : {},
    endMonto : 0,
    postspy : {},
    postqqq : {},
    postmdy : {},
  }

  getDataApi = (etf) => {
    var request = require('request');
    var url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol='+etf+'&outputsize=full&apikey=9KYUWS18QLV2DG8X';

    request.get({
        url: url,
        json: true,
        headers: {'User-Agent': 'request'}
      }, (err, res, data) => {
        if (err) {
          // console.log('Error:', err);
        } else if (res.statusCode !== 200) {
          // console.log('Status:', res.statusCode);
        } else {
          // data is successfully parsed as a JSON object:
          let etf = data['Meta Data']['2. Symbol']
          console.log('POST DATA etf', etf)
          if (etf === 'spy'){
            this.setState({ postspy : data })
          }
          else if (etf === 'qqq'){
            this.setState({ postqqq : data })
          }
          else if (etf === 'mdy'){
            this.setState({ postmdy : data })
          }
        }
    });
  }

  // Extraemos data de api cuando se inserta el modal al dom
  componentDidMount() {

    this.getDataApi('spy');
    this.getDataApi('qqq');
    this.getDataApi('mdy');

  }

  compute = () => {
    let st = new Date();
    let stop = (new Date()).toISOString().slice(0, 10)
    st.setFullYear(st.getFullYear() - this.props.nyear);

    // Info de graficos
    let gdata = {
      title: {
        left: 'left',
        text: 'Monto en US$',
      },
      legend: {
        data: ['Total', 'SPY', 'QQQ', 'MDY']
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      dataZoom: [
        {
          type: 'inside',
          start: 0,
          end: 100
        },
        {
          start: 0,
          end: 100
        }
      ],
      xAxis: {
        type: 'category',
        data: [],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          name : 'Total',
          data: [],
          type: 'line',
          smooth: true,
        },
        {
          name : 'SPY',
          data: [],
          type: 'line',
          smooth: true,
        },
        {
          name : 'QQQ',
          data: [],
          type: 'line',
          smooth: true,
        },
        {
          name : 'MDY',
          data: [],
          type: 'line',
          smooth: true,
        },
      ],
      tooltip: {
        trigger: 'axis',
      },
    }
    let rgdata = {
      title: {
        left: 'left',
        text: 'Rentabilidades',
      },
      legend: {
        data: ['Total', 'SPY', 'QQQ', 'MDY']
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      dataZoom: [
        {
          type: 'inside',
          start: 0,
          end: 100
        },
        {
          start: 0,
          end: 100
        }
      ],
      xAxis: {
        type: 'category',
        data: [],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          name : 'Total',
          data: [],
          type: 'line',
          smooth: true,
        },
        {
          name : 'SPY',
          data: [],
          type: 'line',
          smooth: true,
        },
        {
          name : 'QQQ',
          data: [],
          type: 'line',
          smooth: true,
        },
        {
          name : 'MDY',
          data: [],
          type: 'line',
          smooth: true,
        },
      ],
      tooltip: {
        trigger: 'axis',
      },
    }

    let postspy = this.state.postspy['Time Series (Daily)']
    let postqqq = this.state.postqqq['Time Series (Daily)']
    let postmdy = this.state.postmdy['Time Series (Daily)']

    let dist = false;
    let amountspy = 0;
    let amountqqq = 0;
    let amountmdy = 0;
    let pricespy = 0;
    let priceqqq = 0;
    let pricemdy = 0;
    let montoetf = this.props.monto / 3.0;
    let montospy = 0;
    let montoqqq = 0;
    let montomdy = 0;

    let currenctDate = '';

    while (st.toISOString().slice(0, 10) <= stop) {
      let strDate = st.toISOString().slice(0, 10);
      // La minima fecha de la data es esta
      if ('1999-11-01' <= strDate){
        if (strDate in postspy){
          currenctDate = strDate;
        }
        if (currenctDate !== '') {
          if (dist === false){
            // Cantidad inicial por ETF
            pricespy = postspy[ currenctDate ]['4. close'];
            priceqqq = postqqq[ currenctDate ]['4. close'];
            pricemdy = postmdy[ currenctDate ]['4. close'];

            amountspy = montoetf / pricespy;
            amountqqq = montoetf / priceqqq;
            amountmdy = montoetf / pricemdy;
            dist = true;
          }

          montospy = amountspy * postspy[ currenctDate ]['4. close'];
          montoqqq = amountqqq * postqqq[ currenctDate ]['4. close'];
          montomdy = amountmdy * postmdy[ currenctDate ]['4. close'];

          //Registrando en graficos
          gdata.xAxis.data.push(strDate)
          gdata.series[0].data.push( (montospy + montoqqq + montomdy).toFixed(3) )
          gdata.series[1].data.push(montospy.toFixed(3))
          gdata.series[2].data.push(montoqqq.toFixed(3))
          gdata.series[3].data.push(montomdy.toFixed(3))

          rgdata.xAxis.data.push(strDate)
          rgdata.series[0].data.push( (100 * (montospy + montoqqq + montomdy - this.props.monto) / this.props.monto).toFixed(3) )
          rgdata.series[1].data.push( (100 * (postspy[ currenctDate ]['4. close'] - pricespy) / pricespy).toFixed(3) )
          rgdata.series[2].data.push( (100 * (postqqq[ currenctDate ]['4. close'] - priceqqq) / priceqqq).toFixed(3) )
          rgdata.series[3].data.push( (100 * (postmdy[ currenctDate ]['4. close'] - pricemdy) / pricemdy).toFixed(3) )

          if (strDate === stop) {
            this.setState({
              endMonto : montospy + montoqqq + montomdy
            })
          }

        }

      }
      st.setDate( st.getDate() + 1 )
    }

    this.setState({
      gdata : gdata,
      rgdata : rgdata,
      g :  <ReactECharts option={gdata}/>,
      rg : <ReactECharts option={rgdata}/>
    })
  }

  componentDidUpdate(p) {
    if (this.props.show !== p.show) {
      if (this.props.show === true){
        this.compute();
      }else{
        this.setState({
          g : '',
          rg : ''
        })
      }
    }
  }

  render () {
    return (
      <Modal
        {... this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Inversión en ETF
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>Inversion en el año {(new Date().getFullYear()) - this.props.nyear} a la actualidad</h4>
          <p>
            Tu monto a invertir es de US$ {this.props.monto} y su valor actual es US$ {this.state.endMonto.toFixed(3)}
          </p>
          {this.state.g}
          {this.state.rg}
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Cerrar</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

class WealthForm extends Component {

  state = {
    validation : false,
    monto : '',
    nyear : '',
    modalShow : false
  }

  onSubmit = (e) => {
    let form = e.currentTarget;
    e.preventDefault(); // Prevenir ejecucion comun del formulario
    
    if (form.checkValidity() === false) {
      e.stopPropagation(); // Detener acciones siguientes del formulario
    }

    // Indicamos que pasa a estado de validacion
    this.setState({
      validation : true
    })

    // Mostrar modal si esta OK
    if (form.checkValidity() === true) {
      this.setModalShow(true)
    }

  }

  // Encargado de deteccion de cambio en los insputs
  onChange = (e) => {
    this.setState({
      [ e.target.name ] : e.target.value
    })
    // Combio del texto al input de nyear
    if (e.target.name === 'nyear'){
      e.target.parentNode.childNodes[1].innerHTML = 'el ('.concat((new Date().getFullYear()) - e.target.value, ')')
    }
  }

  setModalShow = (show) => {
    this.setState({
      modalShow : show
    })
  }

  render () {
    return (
      <Form noValidate validated={this.state.validation} onSubmit={this.onSubmit}>
        <Row className="mb-3">
          <Form.Group as={Col} md="4" controlId="validationCustomMonto">
            <Form.Label>Monto</Form.Label>
            <InputGroup hasValidation>
              <InputGroup.Text id="inputGroupPrepend">US$</InputGroup.Text>
              <Form.Control
                type="number"
                min = "0"
                step="0.001"
                placeholder="11,01"
                aria-describedby="inputGroupPrepend"
                required
                name = "monto"
                value={this.state.monto}
                onChange={this.onChange}
              />
              <Form.Control.Feedback type="invalid">
                Indique un Monto
              </Form.Control.Feedback>
            </InputGroup>
          </Form.Group>
        </Row>
        <Row className="mb-3">
          <Form.Group as={Col} md="4" controlId="validationCustomYear">
            <Form.Label>Invertido hace cuantos años</Form.Label>
            <InputGroup hasValidation>
              <Form.Control
                type="number"
                step="1"
                min="1"
                max={(new Date().getFullYear()) - 1999}
                placeholder="1"
                ariadescribedby="inputGroupPospend"
                required
                name = "nyear"
                value={this.state.nyear}
                onChange={this.onChange}
              />
              <InputGroup.Text id="inputGroupPospend">el (YYYY)</InputGroup.Text>
              <Form.Control.Feedback type="invalid">
                Indique el número de años a ser invertido, el máximo es {(new Date().getFullYear()) - 1999} años atrás.
              </Form.Control.Feedback>
            </InputGroup>
          </Form.Group>
        </Row>
        <Button type="submit">Calcular</Button>
        
        <MyVerticallyCenteredModal
          show={this.state.modalShow}
          onHide={() => this.setModalShow(false)}
          nyear={this.state.nyear}
          monto={this.state.monto}
        />
      </Form>
    );
  }
}

class App extends Component {
  render () {
    return (
      <Container className="p-3">
        <Container className="p-5 mb-4 bg-light rounded-3">
          <h1 className="header">Hola!, Fulano de Tal 🎉</h1>
          <br />
          <p> Para revisar cuanto podrías haber ganado invertido un monto en 3 ETF's de la bolsa de NY de manera equitativa desde unos años atrás revísalo aquí.</p>
          <WealthForm />
        </Container>
      </Container>
    )
  }
}

export default App;